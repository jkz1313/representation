<?php
class Mortgage
{
    public $sum;
    public $percent;
    public $years;
    public $aninitialfee;
    public $datefrom;
    public $dateto;

    public function __construct($datefrom, $dateto, $aninitialfee, $sum, $percent, $years)
    {
        $this->datefrom=$datefrom;
        $this->dateto=$dateto;
        $this->aninitialfee=$aninitialfee;
        $this->sum=$sum;
        $this->percent=$percent;
        $this->years=$years;
    }
    public function Annuity() //формула аннуитетного платежа в месяц
    {
        return $this->OnlyLoan()*($this->percentInMonth() *$this->coefficient())/($this->coefficient()-1);
    }
    public function OnlyLoan()
    {
        return $this->sum-$this->aninitialfee;
    }

    public function percentInMonth() //считает процент в месяц от годовой ставки / 100
    {
        return ($this->percent/100)/12;
    }
    public function coefficient() //коэффициент необходимый для формулы
    {
        return pow((1+$this->percentInMonth()), ($this->years*12));
    }
    public function SumPercentageForAllTime() //сумма уплаты процентов банку за весь период
    {
        return (($this->years)*12*$this->Annuity())-$this->OnlyLoan();
    }
    function getPercentInPeriod()
    {
        $period = new Period();
        $pay = $this->OnlyLoan();
        foreach ($period->getMonthInPeriod() as $k => $value)
        {
            //echo $value . '  ' . $this->GetMonthInPeriod()[$k+1]  .'  '. $this->BetweenDay($value, $this->GetMonthInPeriod()[$k+1]).PHP_EOL;
            if ($period->GetMonthInPeriod()[$k+1]!=NULL) {
                if (($period->getYear($value)!=$period->getYear($period->GetMonthInPeriod()[$k+1])) and ((($period->DaysInYear($value))!=($period->DaysInYear($period->GetMonthInPeriod()[$k+1])))))
            {
                //echo "Tut primenyaem dvoinuyu formulu" . " " .
                    $res=$this->DoubleFormula($pay,$value,$k);
            }
            else
            {
               // echo "Применяем моно формулу" . " " .
                    $res=$this->OneFormula($pay,$value,$k);
            }
            } else break;
            $arrayProc[]=$res;
            $pay-=(int) $res;
        }
        return $arrayProc;
    }
    function OneFormula($pay, $value, $k)
    {
        $period = new Period();
        return ($pay*$this->percent*$period->BetweenDay($value, $period->GetMonthInPeriod()[$k+1]))/(100*$period->DaysInYear($value));
    }
    function  DoubleFormula($pay, $value, $k)
    {
        $period = new Period();
        return (($pay * $this->percent * $period->BetweenDay(($period->LastDayYear($value)), $value)) / (100 * $period->DaysInYear($this->datefrom))) +
        (($pay * $this->percent * $period->BetweenDay($period->GetMonthInPeriod()[$k + 1], $period->LastDayYear($value)) / (100 * $period->DaysInYear($this->dateto))));
    }
    function LoanBody()
    {
        foreach ($this->getPercentInPeriod() as $k=>$value)
        {
            $body=$this->Annuity()-$value;
            $arrBody[]=$body;
        }
        return $arrBody;
    }

}
$credit = new Mortgage('2020-03-10', '2020-09-10',500000,2300700, 4.7, 17);
$credit2 = new Mortgage('2020-12-10','2021-09-10',0,1800700,4.7,17);
$credit3 = new Mortgage('2021-02-15','2021-08-15',500000, 2140000, 4.7, 17);
$credit4 = new Mortgage('2023-12-05','2024-06-05',600000,3000000, 5, 20);
$credit5 = new Mortgage('2020-09-01','2026-09-01',500000,2300700, 4.7, 17);
var_dump($credit->getPercentInPeriod());
var_dump($credit->LoanBody());
echo  PHP_EOL;

