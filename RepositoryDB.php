<?php
namespace representation;
use PDO;
use PDOException;
use Exception;
require_once __DIR__ . '/vendor/autoload.php';

class RepositoryDB
{
    public $host;
    public $username;
    public $passwd;


    function __construct($host, $username, $passwd)
    {
        $this->host=$host;
        $this->username=$username;
        $this->passwd=$passwd;

    }

    function including()
    {
        try {
            $dbh = new PDO($this->host,$this->username,$this->passwd);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        echo 'Есть подключение!' . PHP_EOL;;
    }

    function saveInDB(Credit $cred)
    {
        $pdo = new PDO($this->host,$this->username,$this->passwd);
            $stmt = $pdo->prepare("INSERT INTO creditstory (id,datepayment,sumpayment,bodycredit,percent,remainder)
                      VALUES (:id,:datepayment,:sumpayment,:bodycredit,:percent,:remainder)");
            try {
                $pdo->beginTransaction();
                foreach ($cred->getFullArray() as $row)
                {
                    $stmt->execute($row);
                }
                $pdo->commit();
            }catch (Exception $e) {
                $pdo->rollback();
                throw $e;
            }
    }
}
$reg = new RepositoryDB('pgsql:host=localhost;port=5432;dbname=credit','jkz','');
$credit = new Annuity('2020-03-10', '2020-09-10',500000,2300700, 4.7, 17);
$cred = new Credit($credit);
$cred->id=1;
//$reg->saveInDB($cred);
//$reg->saveInDB($credit);
//print_r($zn = $reg->getArray($credit));
//$reg->including();
//$reg->saveInDB($credit);
