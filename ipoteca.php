<?php
class Ipoteca
{
    public $sum;
    public $percent;
    public $years;
    public $aninitialfee;
    public $datefrom;
    public $dateto;

    public function __construct($datefrom, $dateto, $aninitialfee, $sum, $percent, $years)
    {
        $this->datefrom=$datefrom;
        $this->dateto=$dateto;
        $this->aninitialfee=$aninitialfee;
        $this->sum=$sum;
        $this->percent=$percent;
        $this->years=$years;
    }
    public function Annuity() //формула аннуитетного платежа в месяц
    {
        return $this->AmountWithoutInitialPayment()*($this->percentInMonth() *$this->coefficient())/($this->coefficient()-1);
    }
    public function AmountWithoutInitialPayment()
    {
        return $this->sum-$this->aninitialfee;
    }

    public function percentInMonth() //считает процент в месяц от годовой ставки / 100
    {
        return ($this->percent/100)/12;
    }
    public function coefficient() //коэффициент необходимый для формулы
    {
        return pow((1+$this->percentInMonth()), ($this->years*12));
    }
    public function SumPercentageForAllTime() //сумма уплаты процентов банку за весь период
    {
        return (($this->years)*12*$this->Annuity())-$this->AmountWithoutInitialPayment();
    }
    function DaysInYear($year) //подсчет сколько дней в году
    {
        if(( (int) $year % 4)!= 0) {
            return 365;
        } else
            if (( (int) $year % 100)!= 0) {
                return 366;
            }
            else
                if (( (int) $year % 400)!= 0) {
                    return 365;
                }
                else  return 366;
    }
    function getMonthInPeriod() //массив месяцев в заданном периоде
    {
        for ($i=$this->datefrom; $i<=($this->dateto); $i=date('Y-m-d', (strtotime('+1 MONTH', strtotime($i)))))
        {
             $arrayDay[]=$i;
            //echo $i . PHP_EOL;
        }
        return ($arrayDay);

    }
    function BetweenDay($dfrom, $dto) //считает количество дней между заданным месяцем
    {
        return (int) ceil(abs(strtotime($dto) - strtotime($dfrom)) / 86400);
    }
    function getYear($date) // возвращает год заданного периода
    {
        return date('Y', strtotime($date));
    }
        function LastDayYear($date)
        {
            return date('Y-12-31', (strtotime('Y', strtotime($date))));
        }
    function getPercentInPeriod()
    {
        $pay = $this->AmountWithoutInitialPayment();
        foreach ($this->getMonthInPeriod() as $k => $value)
        {
            //echo $value . '  ' . $this->GetMonthInPeriod()[$k+1]  .'  '. $this->BetweenDay($value, $this->GetMonthInPeriod()[$k+1]).PHP_EOL;
            if ($this->GetMonthInPeriod()[$k+1]!=NULL) {
                if (($this->getYear($value)!=$this->getYear($this->GetMonthInPeriod()[$k+1])) and ((($this->DaysInYear($value))!=($this->DaysInYear($this->GetMonthInPeriod()[$k+1])))))
            {
                //echo "Tut primenyaem dvoinuyu formulu" . " " .
                    $res=$this->DoubleFormula($pay,$value,$k);
            }
            else
            {
               // echo "Применяем моно формулу" . " " .
                    $res=$this->OneFormula($pay,$value,$k);
            }
            } else break;
            $arrayProc[]=$res;
            $pay-=(int) $res;
        }
        return $arrayProc;
    }
    function OneFormula($pay, $value, $k)
    {
        return ($pay*$this->percent*$this->BetweenDay($value, $this->GetMonthInPeriod()[$k+1]))/(100*$this->DaysInYear($value));
    }
    function  DoubleFormula($pay, $value, $k)
    {
        return (($pay * $this->percent * $this->BetweenDay(($this->LastDayYear($value)), $value)) / (100 * $this->DaysInYear($this->datefrom))) +
        (($pay * $this->percent * $this->BetweenDay($this->GetMonthInPeriod()[$k + 1], $this->LastDayYear($value)) / (100 * $this->DaysInYear($this->dateto))));
    }
    function LoanBody()
    {
        foreach ($this->getPercentInPeriod() as $k=>$value)
        {
            $body=$this->Annuity()-$value;
            $arrBody[]=$body;
        }
        return $arrBody;
    }

}
$credit = new Ipoteca('2020-03-10', '2020-09-10',500000,2300700, 4.7, 17);
$credit2 = new Ipoteca('2020-12-10','2021-09-10',0,1800700,4.7,17);
$credit3 = new Ipoteca('2021-02-15','2021-08-15',500000, 2140000, 4.7, 17);
$credit4 = new Ipoteca('2023-12-05','2024-06-05',600000,3000000, 5, 20);
$credit5 = new Ipoteca('2020-09-01','2026-09-01',500000,2300700, 4.7, 17);
var_dump($credit->getPercentInPeriod());
var_dump($credit->LoanBody());
echo  PHP_EOL;

