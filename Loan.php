<?php
namespace representation;
interface Loan
{
    function getDatePayment();
    function getSumPayment();
    function getBodyCredit();
    function getPercent();
    function getRemainder();
}