<?php
namespace representation;
class Period
{
    public $datefrom;
    public $dateto;
    function DaysInYear($year) //подсчет сколько дней в году
    {
        if(( (int) $year % 4)!= 0) {
            return 365;
        } else
            if (( (int) $year % 100)!= 0) {
                return 366;
            }
            else
                if (( (int) $year % 400)!= 0) {
                    return 365;
                }
                else  return 366;
    }
    function getMonthInPeriod($datefrom, $dateto) //массив месяцев в заданном периоде
    {
        for ($i=$datefrom; $i<=($dateto); $i=date('Y-m-d', (strtotime('+1 MONTH', strtotime($i)))))
        {
            $arrayDay[]=$i;
            //echo $i . PHP_EOL;
        }
        return ($arrayDay);

    }
    function BetweenDay($dfrom, $dto) //считает количество дней между заданным месяцем
    {
        return (int) ceil(abs(strtotime($dto) - strtotime($dfrom)) / 86400);
    }
    function getYear($date) // возвращает год заданного периода
    {
        return date('Y', strtotime($date));
    }
    function LastDayYear($date)
    {
        return date('Y-12-31', (strtotime('Y', strtotime($date))));
    }
}