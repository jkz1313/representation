<?php
namespace representation;

require_once __DIR__ . '/vendor/autoload.php';

class Differentiated extends Annuity
{
    function  BodyCredit()
    {
        return $this->getOnlyCredit()/($this->years*12);
    }
    function getBodyCredit()
    {
        $count = count($this->getDatePayment());
        return array_fill(0, $count, $this->BodyCredit());
    }

    public function getSumPayment()
    {
        return parent::getPercent();
    }
    function coefficient()
    {
        return ($this->percentInMonth()/100)*12;
    }
    function OneFormula($pay, $value, $k)
    {
        $period = new Period();
        return ($this->BodyCredit())+
        (($pay*$period->BetweenDay($value, $period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k+1])*$this->percent)/($period->DaysInYear($value)*100));
    }
    function DoubleFormula($pay, $value, $k)
    {
        $period = new Period();
        return ($this->BodyCredit())+
        (((($pay*$period->BetweenDay(($period->LastDayYear($value)), $value)*$this->percent))/($period->DaysInYear($this->datefrom)*100))+
        ((($pay*$period->BetweenDay($period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k + 1], $period->LastDayYear($value))*$this->percent))/($period->DaysInYear($this->dateto)*100)));
    }
    function getPercent()
    {
        foreach ($this->getSumPayment() as $value)
        {
            $perc[]=$value-$this->BodyCredit();
        }
        return $perc;
    }
    function getRemainder()
    {
            $payment = $this->getOnlyCredit();
            foreach ($this->getSumPayment() as $k =>$value)
            {
                $sum[]=$payment-$value;
                $payment-=$value;
            }
            return $sum;
    }

}

$test = new Differentiated('2020-08-01', '2021-03-01',500000,2300700, 4.7, 17);
//print_r($test->getDatePayment());
//echo $test->getBodyCredit();
//print_r($test->getSumPayment());
//print_r($test->getPercent());
//print_r($test->getRemainder());
//print_r($test->getFullArray());
//echo $test->getBodyCredit(). PHP_EOL;;
//echo $test->getOnlyCredit(). PHP_EOL;;
