<?php
namespace representation;

require_once __DIR__ . '/vendor/autoload.php';

class Annuity implements Loan
{
    public  $id;
    protected $sum;
    protected $percent;
    protected $years;
    protected $aninitialfee;
    protected $datefrom;
    protected $dateto;

    public function __construct($datefrom, $dateto, $aninitialfee, $sum, $percent, $years)
    {
        $this->datefrom=$datefrom;
        $this->dateto=$dateto;
        $this->aninitialfee=$aninitialfee;
        $this->sum=$sum;
        $this->percent=$percent;
        $this->years=$years;
    }
    public function SumPayment() //формула аннуитетного платежа в месяц
    {
        return $this->getOnlyCredit()*($this->percentInMonth() *$this->coefficient())/($this->coefficient()-1);
    }
    public function getSumPayment()
    {
        $count = count($this->getDatePayment());
        return array_fill(0, $count, $this->SumPayment());
    }
    public function getOnlyCredit()//считает кредит из общей суммы без первоначального взноса
    {
        return $this->sum-$this->aninitialfee;
    }

    protected function percentInMonth() //считает процент в месяц от годовой ставки / 100
    {
        return ($this->percent/100)/12;
    }
    protected function coefficient() //коэффициент необходимый для формулы
    {
        return pow((1+$this->percentInMonth()), ($this->years*12));
    }
    public function SumPercentageForAllTime() //сумма уплаты процентов банку за весь период
    {
        return (($this->years)*12*$this->SumPayment())-$this->getOnlyCredit();
    }
    function getPercent() //возвращает массив где считается сумма кредита в месяц
    {
        $period = new Period();
        $pay = $this->getOnlyCredit();
        foreach ($period->getMonthInPeriod($this->datefrom, $this->dateto) as $k => $value)
        {
            if (isset($period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k+1]))
            {
                if ($this->conditionForFormula($k, $value)==true)
                {
                    $res=$this->DoubleFormula($pay,$value,$k);
                }
                else
                {
                    $res=$this->OneFormula($pay,$value,$k);
                }
            } else break;

            $arrayProc[]=$res;
            $pay-=(int) $res;
        }

        return $arrayProc;
    }
    function conditionForFormula($k,$value)
    {
        $period = new Period();
        if (($period->getYear($value)!=$period->getYear($period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k+1])) and ((($period->DaysInYear($value))!=($period->DaysInYear($period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k+1])))))
        {
            return true;
        }
        else {
            return false;
        }
    }
    function getDatePayment()
    {
        $date = new Period();
        foreach ($date->GetMonthInPeriod($this->datefrom, $this->dateto) as $k => $value)
        {
            if (isset($date->GetMonthInPeriod($this->datefrom, $this->dateto)[$k+1]))
             $array[]=$value;
        }
         return $array;
    }

    function OneFormula($pay, $value, $k)
    {
        $period = new Period();
        return ($pay*$this->percent*$period->BetweenDay($value, $period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k+1]))/(100*$period->DaysInYear($value));
    }
    function  DoubleFormula($pay, $value, $k)
    {
        $period = new Period();
        return (($pay * $this->percent * $period->BetweenDay(($period->LastDayYear($value)), $value)) / (100 * $period->DaysInYear($this->datefrom))) +
        (($pay * $this->percent * $period->BetweenDay($period->GetMonthInPeriod($this->datefrom, $this->dateto)[$k + 1], $period->LastDayYear($value)) / (100 * $period->DaysInYear($this->dateto))));
    }
    function getBodyCredit() //возвращвет массив где считает тело кредита в месяц
    {
        foreach ($this->getPercent() as $k=>$value)
        {
            $body=$this->SumPayment()-$value;
            $arrBody[]=$body;
        }
        return $arrBody;
    }
    function getBodyLoanFull()
    {
        $body=0;
        foreach ($this->getBodyCredit() as $value)
        {
            $body+=$value;
        }
        return $body;
    }
    function getRemainder()
    {
        $payment = $this->getOnlyCredit();
        foreach ($this->getDatePayment() as $k =>$value)
        {
            $sum[]=$payment-$this->SumPayment();
            $payment-=$this->SumPayment();
        }
        return $sum;
    }
    function getFullArray()
    {
        $count = count($this->getDatePayment());
        $arrId = array_fill(0, $count, $this->id);

        $items =[];
        for ($x = 0; $x < $count; $x++) {
            $item[] = $arrId[$x];
            $item[]= $this->getDatePayment()[$x];
            $item[] = $this->getSumPayment()[$x];
            $item[] = $this->getBodyCredit()[$x];
            $item[] = $this->getPercent()[$x];
            $item[] = $this->getRemainder()[$x];
            $items[] = $item;
            $item=[];
        }
        return $items;
    }

}
$credit = new Annuity('2020-03-10', '2020-09-10',500000,2300700, 4.7, 17);
$credit2 = new Annuity('2020-12-10','2021-09-10',0,1800700,4.7,17);
$credit3 = new Annuity('2021-02-15','2021-08-15',500000, 2140000, 4.7, 17);
$credit4 = new Annuity('2023-12-05','2024-06-05',600000,3000000, 5, 20);
$credit5 = new Annuity('2020-09-01','2026-09-01',500000,2300700, 4.7, 17);
//print_r($credit->getFullArray()) . PHP_EOL;

echo  PHP_EOL;

